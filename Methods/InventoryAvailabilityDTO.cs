﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    public partial class InventoryAvailabilityDTO
    {

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        
        public int certificateType { get; set; }
        public int totalAvailableCertificates { get; set; }
        public List<Journey> journeys { get; set; }


        public partial class Journey

        {
        public int journeyIndex { get; set; }
        public int required { get; set; }
        public int inventoryAvailability { get; set; }
        public int availableCertificatesForJourney { get; set; }
        }
    }
}

