﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    public partial class CabinUpgradeStatusDTO
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
            public string pnr { get; set; }
            public string status { get; set; }
            public string message { get; set; }
            public List<Journey> journeys { get; set; }
 
        public partial class Journey
        {
            public int journeyIndex { get; set; }
            public bool isIROP { get; set; }
            public bool isUpgradedUsingCertificate { get; set; }
        }

       


    }
}
