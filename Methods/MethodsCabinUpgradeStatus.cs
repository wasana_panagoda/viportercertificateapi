﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace Methods
{
   public class MethodsCabinUpgradeStatus
    {
        public int GetMethod(String PNR)
        {
            var client = new RestClient("https://qa-porterbusiness.flyporter.com/flightchange.api/flightChange/CabinUpgradeStatus");
            var request = new RestRequest(Method.GET);
            request.AddParameter("recordLocator", PNR);
            IRestResponse response = client.Execute(request);
            HttpStatusCode statusCode = response.StatusCode;
            int nStatusCode = (int)statusCode;
            return nStatusCode;

        }


        public CabinUpgradeStatusDTO Deserializer(String PNR)
        {
            var client = new RestClient("https://qa-porterbusiness.flyporter.com/flightchange.api/flightChange/CabinUpgradeStatus");
            var request = new RestRequest(Method.GET);
            request.AddParameter("recordLocator", PNR);
            request.RequestFormat = DataFormat.Json;

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            var status = JsonConvert.DeserializeObject<CabinUpgradeStatusDTO>(content);
            return status;


        }
    }
}
