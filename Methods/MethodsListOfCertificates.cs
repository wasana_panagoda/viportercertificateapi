﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace Methods
{
    public class MethodsListOfCertificates
    {
        public int GetMethod(long ID)
        {
            var client = new RestClient("https://qa-porterbusiness.flyporter.com/certificate.api/v1/Certificates");
            var request = new RestRequest(Method.GET);
            request.AddParameter("UserId", ID);
            IRestResponse response = client.Execute(request);
            HttpStatusCode statusCode = response.StatusCode;
            int nStatusCode = (int)statusCode;
            return nStatusCode;

        }

        public ListOfCertificatesDTO Deserializer(long ID)
        {
            var client = new RestClient("https://qa-porterbusiness.flyporter.com/certificate.api/v1/Certificates");
            var request = new RestRequest(Method.GET);
            request.AddParameter("UserId", ID);

            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);
            var content = response.Content;

            var list = JsonConvert.DeserializeObject<ListOfCertificatesDTO>(content);
            return list;


        }

   
    }
}
