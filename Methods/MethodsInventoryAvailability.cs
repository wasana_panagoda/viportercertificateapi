﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;

namespace Methods
{
    public class MethodsInventoryAvailability
    {
        
            public int GetMethod(long ID, String PNR)
            {
                var client = new RestClient("https://qa-porterbusiness.flyporter.com/Flightavailability.api/FlightAvailability/CabinUpgrade");
                var request = new RestRequest(Method.GET);
                request.AddParameter("userId", ID);
                request.AddParameter("recordLocator", PNR);
                request.AddParameter("certificateType", "CabinUpgrade");
                IRestResponse response = client.Execute(request);
                HttpStatusCode statusCode = response.StatusCode;
                int nStatusCode = (int)statusCode;
                return nStatusCode;

            }


            public InventoryAvailabilityDTO Deserializer(long ID, String PNR)
            {
                var client = new RestClient("https://qa-porterbusiness.flyporter.com/Flightavailability.api/FlightAvailability/CabinUpgrade");
                var request = new RestRequest(Method.GET);
                request.AddParameter("userId", ID);
                request.AddParameter("recordLocator", PNR);
                request.AddParameter("certificateType", "CabinUpgrade");
                request.RequestFormat = DataFormat.Json;

                IRestResponse response = client.Execute(request);
                var content = response.Content;

                var status = JsonConvert.DeserializeObject<InventoryAvailabilityDTO>(content);
                return status;


            }
        
    }


}
