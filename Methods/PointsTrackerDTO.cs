﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base
{
    public class PointsTrackerDTO
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        
            public int start { get; set; }
            public int pointsInInterval { get; set; }
            public int end { get; set; }
        

    }
}
