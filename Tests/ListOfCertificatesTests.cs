﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Methods;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class ListOfCertificatesTests
    {
        [TestMethod]
        public void VerifyAPIResponseAsStatusOKListOfCertificates()
        {
            var methods = new MethodsListOfCertificates();
            var code =  methods.GetMethod(4030013296);

            Assert.AreEqual(200, code);
        }

        [TestMethod]
        public void VerifyTypeOfCertificatesInListOfCertificates()
        {
            var methods = new MethodsListOfCertificates();
            var response = methods.Deserializer(4030013296);

            Assert.AreEqual(0, response.certificates[0].type);
        }

        [TestMethod]
        public void VerifyVoucherReferenceNumberOfCertificates()
        {
            var methods = new MethodsListOfCertificates();
            var response = methods.Deserializer(4030013296);

            Assert.AreEqual("35343920667600001", response.certificates[0].voucherReferenceNumber);
        }

        [TestMethod]
        public void VerifyIssuedDateUTCOfCertificates()
        {
            var methods = new MethodsListOfCertificates();
            var response = methods.Deserializer(4030013296);

            Assert.AreEqual("2022-03-16T00:31:39.58", response.certificates[0].issuedDateUTC);
        }
        [TestMethod]
        public void VerifyExpiryDateUTCOfCertificates()
        {
            var methods = new MethodsListOfCertificates();
            var response = methods.Deserializer(4030013296);

            Assert.AreEqual("2023-03-17T03:59:00", response.certificates[0].expiryDateUTC);
        }
        [TestMethod]
        public void VerifyNumberOfCertificates()
        {
            var methods = new MethodsListOfCertificates();
            var response = methods.Deserializer(4030013296);

            Assert.AreEqual(176, response.certificates.Count);
        }
    }
}
