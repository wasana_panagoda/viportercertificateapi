﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Methods;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class InventoryAvailabilityTests
    {
        [TestMethod]
        public void VerifyAPIResponseAsStatusOKInventoryAvailability()
        {
            var methods = new MethodsInventoryAvailability();
            var code = methods.GetMethod(8500012784, "DC5DSK");
            Assert.AreEqual(200, code);
        }
        [TestMethod]
        public void VerifyInventoryAvailabilityQ400Return()
        {
            var methods = new MethodsInventoryAvailability();
            var response = methods.Deserializer(8500012784, "DC5DSK");

            Assert.AreEqual(0, response.certificateType);
            Assert.AreEqual(32, response.totalAvailableCertificates);

            Assert.AreEqual(3, response.journeys[0].required);
            Assert.AreEqual(0, response.journeys[0].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[0].availableCertificatesForJourney);

            Assert.AreEqual(3, response.journeys[1].required);
            Assert.AreEqual(0, response.journeys[1].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[1].availableCertificatesForJourney);


        }

        [TestMethod]
        public void VerifyInventoryAvailabilityQ400OneWay()
        {
            var methods = new MethodsInventoryAvailability();
            var response = methods.Deserializer(8500012784, "DC5DSK");

            Assert.AreEqual(0, response.certificateType);
            Assert.AreEqual(32, response.totalAvailableCertificates);

            Assert.AreEqual(3, response.journeys[0].required);
            Assert.AreEqual(0, response.journeys[0].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[0].availableCertificatesForJourney);

            Assert.AreEqual(3, response.journeys[1].required);
            Assert.AreEqual(0, response.journeys[1].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[1].availableCertificatesForJourney);


        }

        [TestMethod]
        public void VerifyInventoryAvailabilityE2Return()
        {
            var methods = new MethodsInventoryAvailability();
            var response = methods.Deserializer(8500012784, "DC5DSK");

            Assert.AreEqual(0, response.certificateType);
            Assert.AreEqual(32, response.totalAvailableCertificates);

            Assert.AreEqual(3, response.journeys[0].required);
            Assert.AreEqual(0, response.journeys[0].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[0].availableCertificatesForJourney);

            Assert.AreEqual(3, response.journeys[1].required);
            Assert.AreEqual(0, response.journeys[1].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[1].availableCertificatesForJourney);


        }

        [TestMethod]
        public void VerifyInventoryAvailabilityE2OneWay()
        {
            var methods = new MethodsInventoryAvailability();
            var response = methods.Deserializer(8500012784, "DC5DSK");

            Assert.AreEqual(0, response.certificateType);
            Assert.AreEqual(32, response.totalAvailableCertificates);

            Assert.AreEqual(3, response.journeys[0].required);
            Assert.AreEqual(0, response.journeys[0].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[0].availableCertificatesForJourney);

            Assert.AreEqual(3, response.journeys[1].required);
            Assert.AreEqual(0, response.journeys[1].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[1].availableCertificatesForJourney);


        }

        [TestMethod]
        public void VerifyInventoryAvailabilityExpiryDateLogicQ400()
        {
            var methods = new MethodsInventoryAvailability();
            var response = methods.Deserializer(6950013090, "OY1IWT");

            Assert.AreEqual(0, response.certificateType);
            Assert.AreEqual(32, response.totalAvailableCertificates);

            Assert.AreEqual(3, response.journeys[0].required);
            Assert.AreEqual(0, response.journeys[0].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[0].availableCertificatesForJourney);

            Assert.AreEqual(3, response.journeys[1].required);
            Assert.AreEqual(0, response.journeys[1].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[1].availableCertificatesForJourney);


        }

        [TestMethod]
        public void VerifyInventoryAvailabilityExpiryDateLogicE2()
        {
            var methods = new MethodsInventoryAvailability();
            var response = methods.Deserializer(8500012784, "S6YYYC");

            Assert.AreEqual(0, response.certificateType);
            Assert.AreEqual(34, response.totalAvailableCertificates);

            Assert.AreEqual(5, response.journeys[0].required);
            Assert.AreEqual(0, response.journeys[0].inventoryAvailability);
            Assert.AreEqual(0, response.journeys[0].availableCertificatesForJourney);

        }


    }
}
