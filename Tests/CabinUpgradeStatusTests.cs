﻿using System;
using Methods;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class CabinUpgradeStatusTests
    {
        [TestMethod]
        public void VerifyAPIResponseAsStatusOKCabinUpgradeStatus()
        {
            var methods = new MethodsCabinUpgradeStatus();
            var code = methods.GetMethod("M37GUY");

            Assert.AreEqual(200, code);
        }
        [TestMethod]
        public void VerifyCabinUpgradeStatusUpgradedFlightIROPedWithin7Days()
        {
            var methods = new MethodsCabinUpgradeStatus();
            var response = methods.Deserializer("M37GUY");
            
            Assert.AreEqual(0, response.journeys[0].journeyIndex);
            Assert.AreEqual(false, response.journeys[0].isIROP);
            Assert.AreEqual(true, response.journeys[0].isUpgradedUsingCertificate);
            

        }
        [TestMethod]
        public void VerifyCabinUpgradeStatusNonUpgradedFlightIROPedWithin7Days()
        {
            var methods = new MethodsCabinUpgradeStatus();
            var response = methods.Deserializer("GECU2B");

            Assert.AreEqual(0, response.journeys[0].journeyIndex);
            Assert.AreEqual(false, response.journeys[0].isIROP);
            Assert.AreEqual(false, response.journeys[0].isUpgradedUsingCertificate);
            Assert.AreEqual(1, response.journeys[1].journeyIndex);
            Assert.AreEqual(false, response.journeys[1].isIROP);
            Assert.AreEqual(false, response.journeys[1].isUpgradedUsingCertificate);


        }
        [TestMethod]
        public void VerifyCabinUpgradeStatusUpgradedFlightIROPedOutside7Days()
        {
            var methods = new MethodsCabinUpgradeStatus();
            var response = methods.Deserializer("I5BQPC");

            Assert.AreEqual(0, response.journeys[0].journeyIndex);
            Assert.AreEqual(false, response.journeys[0].isIROP);
            Assert.AreEqual(true, response.journeys[0].isUpgradedUsingCertificate);


        }
        [TestMethod]
        public void VerifyCabinUpgradeStatusNonUpgradedFlightIROPedOutside7Days()
        {
            var methods = new MethodsCabinUpgradeStatus();
            var response = methods.Deserializer("U3N2RY");

            Assert.AreEqual(0, response.journeys[0].journeyIndex);
            Assert.AreEqual(false, response.journeys[0].isIROP);
            Assert.AreEqual(false, response.journeys[0].isUpgradedUsingCertificate);


        }
        [TestMethod]
        public void VerifyCabinUpgradeStatusUpgradedFlightNotIROPed()
        {
            var methods = new MethodsCabinUpgradeStatus();
            var response = methods.Deserializer("DY7U6N");

            Assert.AreEqual(0, response.journeys[0].journeyIndex);
            Assert.AreEqual(false, response.journeys[0].isIROP);
            Assert.AreEqual(true, response.journeys[0].isUpgradedUsingCertificate);


        }
        [TestMethod]
        public void VerifyCabinUpgradeStatusNonUpgradedFlightNotIROPed()
        {
            var methods = new MethodsCabinUpgradeStatus();
            var response = methods.Deserializer("ICS7PX");

            Assert.AreEqual(0, response.journeys[0].journeyIndex);
            Assert.AreEqual(false, response.journeys[0].isIROP);
            Assert.AreEqual(false, response.journeys[0].isUpgradedUsingCertificate);

            Assert.AreEqual(1, response.journeys[1].journeyIndex);
            Assert.AreEqual(false, response.journeys[1].isIROP);
            Assert.AreEqual(false, response.journeys[1].isUpgradedUsingCertificate);


        }
        [TestMethod]
        public void VerifyCabinUpgradeStatusOutboundJourneyUpgradedIROPedWithin7Days()
        {
            var methods = new MethodsCabinUpgradeStatus();
            var response = methods.Deserializer("H983PT");

            Assert.AreEqual(0, response.journeys[0].journeyIndex);
            Assert.AreEqual(true, response.journeys[0].isIROP);
            Assert.AreEqual(true, response.journeys[0].isUpgradedUsingCertificate);

            Assert.AreEqual(1, response.journeys[1].journeyIndex);
            Assert.AreEqual(false, response.journeys[1].isIROP);
            Assert.AreEqual(false, response.journeys[1].isUpgradedUsingCertificate);


        }
        [TestMethod]
        public void VerifyCabinUpgradeStatusFreedomFareUpgradedIROPedWithin7Days()
        {
            var methods = new MethodsCabinUpgradeStatus();
            var response = methods.Deserializer("O3VMGV");

            Assert.AreEqual(0, response.journeys[0].journeyIndex);
            Assert.AreEqual(true, response.journeys[0].isIROP);
            Assert.AreEqual(true, response.journeys[0].isUpgradedUsingCertificate);


        }
    }
}
