﻿using System;
using System.Net;
using Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;



namespace Tests
{
    [TestClass]
    public class PointsTrackerTests
    {
       

        [TestMethod]
        public void VerifyAPIResponseAsStatusOKPointsTracker()
        {
            //Arrange, Act, Assert
            var methods = new MethodsPointsTracker();
            var code = methods.GetMethod(4030013296);

            Assert.AreEqual(200, code);

        }

        [TestMethod]
        public void VerifyResponseWhenUserHas2999Points()
        {
            var methods = new MethodsPointsTracker();
            var response = methods.Deserializer(5160013156);
            Assert.AreEqual(0, response.start);
            Assert.AreEqual(2999, response.pointsInInterval);
            Assert.AreEqual(3000, response.end);
        }
        
        
        [TestMethod]
        public void VerifyResponseWhenUserHas3000Points()
        {
            var methods = new MethodsPointsTracker();
            var response = methods.Deserializer(5630013285);
            Assert.AreEqual(0, response.start);
            Assert.AreEqual(0, response.pointsInInterval);
            Assert.AreEqual(2000, response.end);
        }
        
        [TestMethod]
        public void VerifyResponseWhenUserHas3001Points()
        {
            var methods = new MethodsPointsTracker();
            var response = methods.Deserializer(9640013403);
            Assert.AreEqual(0, response.start);
            Assert.AreEqual(1, response.pointsInInterval);
            Assert.AreEqual(2000, response.end);
        }
        
        [TestMethod]
        public void VerifyResponseWhenUserHas8999Points()
        {
            var methods = new MethodsPointsTracker();
            var response = methods.Deserializer(3240013392);
            Assert.AreEqual(0, response.start);
            Assert.AreEqual(1999, response.pointsInInterval);
            Assert.AreEqual(2000, response.end);
        }
        
        [TestMethod]
        public void VerifyResponseWhenUserHas9000Points()
        {
            var methods = new MethodsPointsTracker();
            var response = methods.Deserializer(5330013322);
            Assert.AreEqual(0, response.start);
            Assert.AreEqual(0, response.pointsInInterval);
            Assert.AreEqual(2000, response.end);
        }
        
        [TestMethod]
        public void VerifyResponseWhenUserHas9100Points()
        {
            var methods = new MethodsPointsTracker();
            var response = methods.Deserializer(2020013112);
            Assert.AreEqual(0, response.start);
            Assert.AreEqual(100, response.pointsInInterval);
            Assert.AreEqual(2000, response.end);
        }
        
        [TestMethod]
        public void VerifyResponseWhenUserHas0Points()
        {
            var methods = new MethodsPointsTracker();
            var response = methods.Deserializer(3250013554);
            Assert.AreEqual(0, response.start);
            Assert.AreEqual(0, response.pointsInInterval);
            Assert.AreEqual(3000, response.end);
        }
        
        
        [TestMethod]
        public void VerifyResponseWhenUserHas100000Points()
        {
            var methods = new MethodsPointsTracker();
            var response = methods.Deserializer(2640013705);
            Assert.AreEqual(0, response.start);
            Assert.AreEqual(1000, response.pointsInInterval);
            Assert.AreEqual(2000, response.end);
        }





    }
}
